const input = document.querySelector("#inp");

document.querySelector("#bt1").addEventListener("click", (event) => {
  input.value = (Number(input.value.trim()) || 0) - 1;
});
document.querySelector("#bt2").addEventListener("click", (event) => {
  input.value = (Number(input.value.trim()) || 0) + 1;
});
